﻿DECLARE @DB varchar(50)
  SELECT @DB = DB_NAME()

DECLARE @SQLString VARCHAR(200)
SET @SQLString = '  ALTER DATABASE [' + @DB + '] SET ALLOW_SNAPSHOT_ISOLATION ON'
EXEC(@SqlString)

SET @SQLString = '  ALTER DATABASE [' + @DB + '] SET READ_COMMITTED_SNAPSHOT ON'
