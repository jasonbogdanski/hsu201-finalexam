﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConferenceApp.UI.Infrastructure.AutoMapper;
using NHibernate;
using StructureMap.AutoMocking;
using StructureMap;
using StructureMap.Graph;
using IntegrationTests.DependencyResolution;

namespace IntegrationTests
{
    public class IntegrationTestBase
    {
        private readonly IContainer _container;
        private readonly ISessionFactory _sessionFactory;
        protected ISession _session;

        public IntegrationTestBase()
        {
            _container = IoC.Initialize();
            _sessionFactory = _container.GetInstance<ISessionFactory>();
            AutoMapperBootStrapper.Initialize();
        }

        public void SetUp()
        {
            var databaseDeleter = new DatabaseDeleter(_sessionFactory);
            databaseDeleter.DeleteAllData();
            _session = _sessionFactory.OpenSession();
        }

        public void TearDown()
        {
            _session.Flush();
            _session.Clear();
        }
    }
}
