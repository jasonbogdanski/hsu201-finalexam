﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConferenceApp.UI.Features.Conference;
using Should;

namespace IntegrationTests.UI.Features.Conference
{
    public class AddEditQueryHandlerTests : IntegrationTestBase
    {
        public void should_return_conference()
        {
            var conference = new Core.Domain.Conference
            {
                Name = "Dude",
                HashTag = "#Dude",
                StartDate = Convert.ToDateTime("2/2/2015"),
                EndDate = Convert.ToDateTime("3/3/2015"),
                Cost = 200
            };

            _session.Save(conference);
            _session.Flush();

            var editQuery = new AddEditQueryHandler(_session);
            var viewModel = editQuery.Handle(new AddEditQuery {Id = conference.Id});
            viewModel.Id.ShouldEqual(conference.Id);
        }
    }
}
