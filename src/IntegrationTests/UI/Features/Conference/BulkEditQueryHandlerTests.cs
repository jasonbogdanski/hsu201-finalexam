﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConferenceApp.UI.Features.Conference;
using Should;

namespace IntegrationTests.UI.Features.Conference
{
    public class BulkEditQueryHandlerTests : IntegrationTestBase
    {
        public void should_return_bulk_edit_view_model()
        {
            _session.Save(new Core.Domain.Conference
            {
                Name = "Nice",
                HashTag = "#Nice",
                StartDate = Convert.ToDateTime("3/1/2015"),
                EndDate = Convert.ToDateTime("4/3/2015"),
                Cost = 300
            });

            _session.Save(new Core.Domain.Conference
            {
                Name = "Dude",
                HashTag = "#Dude",
                StartDate = Convert.ToDateTime("2/2/2015"),
                EndDate = Convert.ToDateTime("3/3/2015"),
                Cost = 200
            });

            _session.Save(new Core.Domain.Conference
            {
                Name = "Conf",
                HashTag = "#Conf",
                StartDate = Convert.ToDateTime("1/1/2015"),
                EndDate = Convert.ToDateTime("1/3/2015"),
                Cost = 100
            });

            _session.Flush();

            var viewModel = new BulkEditQueryHandler(_session).Handle(new BulkEditQuery());

            viewModel.Details.Count.ShouldEqual(3);
            viewModel.Details.FirstOrDefault().ConferenceName.ShouldEqual("Conf");
        }
    }
}
