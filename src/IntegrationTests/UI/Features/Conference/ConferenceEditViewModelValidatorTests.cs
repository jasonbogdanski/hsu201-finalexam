﻿using System;
using ConferenceApp.UI.Features.Conference;
using FluentValidation.TestHelper;

namespace IntegrationTests.UI.Features.Conference
{
    public class ConferenceEditViewModelValidatorTests : IntegrationTestBase
    {
        public void should_validate_required_fields()
        {
            var validator = new ConferenceEditViewModelValidator(_session);
            validator.ShouldHaveValidationErrorFor(conf => conf.ConferenceName, new ConferenceEditViewModel());
            validator.ShouldHaveValidationErrorFor(conf => conf.HashTag, new ConferenceEditViewModel());
            validator.ShouldHaveValidationErrorFor(conf => conf.StartDate, new ConferenceEditViewModel());
            validator.ShouldHaveValidationErrorFor(conf => conf.EndDate, new ConferenceEditViewModel());
            validator.ShouldHaveValidationErrorFor(conf => conf.Cost, new ConferenceEditViewModel());
        }

        public void should_validate_when_startdate_is_greater_than_enddate()
        {
            var validator = new ConferenceEditViewModelValidator(_session);
            validator.ShouldHaveValidationErrorFor(conf => conf.EndDate,
                new ConferenceEditViewModel
                {
                    StartDate = Convert.ToDateTime("1/2/2015"),
                    EndDate = Convert.ToDateTime("1/2/2014")
                }
                    );
        }

        public void should_validate_when_there_is_a_duplicate_conference_name()
        {
            const string conferenceName = "Dude";

            _session.Save(new Core.Domain.Conference
            {
                Name = conferenceName,
                HashTag = "#Dude",
                StartDate = Convert.ToDateTime("2/2/2015"),
                EndDate = Convert.ToDateTime("3/3/2015"),
                Cost = 200
            });

            _session.Flush();

            var validator = new ConferenceEditViewModelValidator(_session);
            validator.ShouldHaveValidationErrorFor(conf => conf.ConferenceName, new ConferenceEditViewModel { ConferenceName = conferenceName });
        }

    }
}
