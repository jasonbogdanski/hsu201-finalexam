﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConferenceApp.UI.Features.Conference;
using Should;

namespace IntegrationTests.UI.Features.Conference
{
    public class BulkEditHandlerTests : IntegrationTestBase
    {
        public void should_update_all_conferences()
        {
            _session.Save(new Core.Domain.Conference
            {
                Name = "Nice",
                HashTag = "#Nice",
                StartDate = Convert.ToDateTime("3/1/2015"),
                EndDate = Convert.ToDateTime("4/3/2015"),
                Cost = 300
            });

            _session.Save(new Core.Domain.Conference
            {
                Name = "Dude",
                HashTag = "#Dude",
                StartDate = Convert.ToDateTime("2/2/2015"),
                EndDate = Convert.ToDateTime("3/3/2015"),
                Cost = 200
            });

            _session.Save(new Core.Domain.Conference
            {
                Name = "Conf",
                HashTag = "#Conf",
                StartDate = Convert.ToDateTime("1/1/2015"),
                EndDate = Convert.ToDateTime("1/3/2015"),
                Cost = 100
            });

            _session.Flush();
            var viewModel = new BulkEditQueryHandler(_session).Handle(new BulkEditQuery());
            var firstViewModel = viewModel.Details[0];
            var secondViewModel = viewModel.Details[1];
            var thirdViewModel = viewModel.Details[2];

            firstViewModel.ConferenceName = "Conf Fest";
            secondViewModel.Cost = 600;
            thirdViewModel.EndDate = Convert.ToDateTime("1/2/2020");

            var handler = new BulkEditHandler(new AddEditHandler(_session));
            handler.Handle(viewModel);
            _session.Flush();

            var viewModel2 = new BulkEditQueryHandler(_session).Handle(new BulkEditQuery());
            viewModel2.Details[0].ConferenceName.ShouldEqual(firstViewModel.ConferenceName);
            viewModel2.Details[1].Cost.ShouldEqual(secondViewModel.Cost);
            viewModel2.Details[2].EndDate.ShouldEqual(thirdViewModel.EndDate);
        }
    }
}
