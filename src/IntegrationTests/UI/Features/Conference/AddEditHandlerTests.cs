﻿using System;
using ConferenceApp.UI.Features.Conference;
using Should;

namespace IntegrationTests.UI.Features.Conference
{
    public class AddEditHandlerTests : IntegrationTestBase
    {
        public void should_save_conference()
        {
            var addConferenceHandler = new AddEditHandler(_session);

            var conferenceEditViewModel = new ConferenceEditViewModel
            {
                ConferenceName = "Con fest",
                HashTag = "#hash",
                StartDate = Convert.ToDateTime("1/1/2015"),
                EndDate = Convert.ToDateTime("1/3/2015"),
                Cost = 100
            };
            addConferenceHandler.Handle(conferenceEditViewModel);

            _session.Flush();
            var conference = _session.QueryOver<Core.Domain.Conference>().SingleOrDefault();
            conference.Name.ShouldEqual(conferenceEditViewModel.ConferenceName);
            conference.HashTag.ShouldEqual(conferenceEditViewModel.HashTag);
            conference.StartDate.ShouldEqual(conferenceEditViewModel.StartDate);
            conference.EndDate.ShouldEqual(conferenceEditViewModel.EndDate);
            conference.Cost.ShouldEqual(conferenceEditViewModel.Cost.Value);       
        }

        public void should_update_conference()
        {
            var addConferenceHandler = new AddEditHandler(_session);

            var conferenceEditViewModel = new ConferenceEditViewModel
            {
                ConferenceName = "Con fest",
                HashTag = "#hash",
                StartDate = Convert.ToDateTime("1/1/2015"),
                EndDate = Convert.ToDateTime("1/3/2015"),
                Cost = 100
            };
            addConferenceHandler.Handle(conferenceEditViewModel);
            _session.Flush();
            var conference = _session.QueryOver<Core.Domain.Conference>().SingleOrDefault();
            conferenceEditViewModel.Id = conference.Id;

            conferenceEditViewModel.ConferenceName = "Stuff";
            conferenceEditViewModel.HashTag = "#dude";
            conferenceEditViewModel.StartDate = Convert.ToDateTime("2/1/2015");
            conferenceEditViewModel.EndDate = Convert.ToDateTime("2/3/2015");
            conferenceEditViewModel.Cost = 200;

            addConferenceHandler.Handle(conferenceEditViewModel);
            _session.Flush();
            conference = _session.QueryOver<Core.Domain.Conference>().SingleOrDefault();

            conference.Name.ShouldEqual(conferenceEditViewModel.ConferenceName);
            conference.HashTag.ShouldEqual(conferenceEditViewModel.HashTag);
            conference.StartDate.ShouldEqual(conferenceEditViewModel.StartDate);
            conference.EndDate.ShouldEqual(conferenceEditViewModel.EndDate);
            conference.Cost.ShouldEqual(conferenceEditViewModel.Cost.Value);
        }
    }
}
