﻿using ConferenceApp.UI.DependencyResolution;
using ConferenceApp.UI.Features.Conference;
using Core.CommandProcessorSupport;
using Should;
using StructureMap;

namespace IntegrationTests.UI.DependencyResolution
{
    public class DefaultRegistryTests
    {
        public void should_create_instance_of_handler()
        {
            var container = new Container(c => c.AddRegistry<DefaultRegistry>());
            var handler = container.GetInstance<ICommandHandler<ConferenceEditViewModel, ConferenceEditViewModel>>();
            handler.ShouldBeType(typeof (AddEditHandler));
        }
    }
}
