﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConferenceApp.UI.DependencyResolution;
using NHibernate;
using StructureMap;
using StructureMap.Graph;

namespace IntegrationTests.DependencyResolution
{
    public static class IoC
    {    
        public static IContainer Initialize()
        {
            return new Container(c => c.AddRegistry<DefaultRegistry>());
        }
    }
}
