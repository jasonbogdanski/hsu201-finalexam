﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ConferenceApp.UI.Startup))]
namespace ConferenceApp.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
