﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Core.CommandProcessorSupport;
using NHibernate;

namespace ConferenceApp.UI.Features.Conference
{
    using Core.Domain;

    public class BulkEditQueryHandler : ICommandHandler<BulkEditQuery, ConferenceBulkEditViewModel>
    {
        private readonly ISession _session;

        public BulkEditQueryHandler(ISession session)
        {
            _session = session;
        }

        public ConferenceBulkEditViewModel Handle(BulkEditQuery viewModel)
        {
            return new ConferenceBulkEditViewModel
            {
                Details = Mapper.Map<IList<Conference>, IList<ConferenceEditViewModel>>(_session.QueryOver<Conference>().OrderBy(x => x.Name).Asc.List())
            };
        }
    }
}