﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Core.CommandProcessorSupport;

namespace ConferenceApp.UI.Features.Conference
{
    public class ConferenceSummaryViewModel
    {
        public Guid Id { get; set; }
        public string ConferenceName { get; set; }
        public string HashTag { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal? Cost { get; set; }
        public int AttendeeCount { get; set; }
    }
}