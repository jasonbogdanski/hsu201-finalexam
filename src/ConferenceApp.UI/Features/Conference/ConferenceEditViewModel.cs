﻿using System.ComponentModel.DataAnnotations;

namespace ConferenceApp.UI.Features.Conference
{
    using System;
    using System.ComponentModel;
    using System.Web.Mvc;
    using Core.CommandProcessorSupport;
    using FluentValidation;
    using FluentValidation.Attributes;
    using NHibernate;

    [Validator(typeof(ConferenceEditViewModelValidator))]
    public class ConferenceEditViewModel : ICommand, ICommandResult
    {
        public Guid? Id { get; set; }

        [DisplayName("Name")]
        public string ConferenceName { get; set; }

        [DisplayName("Hash Tag")]
        public string HashTag { get; set; }

        [DisplayName("Start Date")]
        public DateTime? StartDate { get; set; }

        [DisplayName("End Date")]
        public DateTime? EndDate { get; set; }

        public decimal? Cost { get; set; }

        [HiddenInput]
        [DisplayName("Attendees")]
        public int AttendeeCount { get; set; }

    }
}