﻿using Core.CommandProcessorSupport;
using NHibernate;
using Core.Domain;

namespace ConferenceApp.UI.Features.Conference
{
    public class AddEditHandler : ICommandHandler<ConferenceEditViewModel, ConferenceEditViewModel>
    {
        private readonly ISession _session;

        public AddEditHandler(ISession session)
        {
            _session = session;
        }

        public ConferenceEditViewModel Handle(ConferenceEditViewModel viewModel)
        {
            var conference = viewModel.Id.HasValue
                ? _session.Get<Core.Domain.Conference>(viewModel.Id.Value)
                : new Core.Domain.Conference();

            conference.Name = viewModel.ConferenceName;
            conference.StartDate = viewModel.StartDate;
            conference.EndDate = viewModel.EndDate;
            conference.HashTag = viewModel.HashTag;
            if (viewModel.Cost != null) conference.Cost = viewModel.Cost.Value;

            _session.SaveOrUpdate(conference);

            return viewModel;
        }
    }
}
