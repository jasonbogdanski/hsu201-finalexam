﻿
using System.Linq;

namespace ConferenceApp.UI.Features.Conference
{
    using Core.CommandProcessorSupport;
    using Core.Domain;
    using NHibernate;
    using AutoMapper;
    using System.Collections.Generic;

    public class IndexQueryHandler : ICommandHandler<IndexQuery, ConferenceIndexViewModel>
    {
        private readonly ISession _session;

        public IndexQueryHandler(ISession session)
        {
            _session = session;
        }

        public ConferenceIndexViewModel Handle(IndexQuery viewModel)
        {
            var model = new ConferenceIndexViewModel
            {
                Details = Mapper.Map<IList<Conference>, IList<ConferenceSummaryViewModel>>(_session.QueryOver<Conference>().OrderBy(x=>x.Name).Asc.List())
            };

            return model;
        }
    }
}