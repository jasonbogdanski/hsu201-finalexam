﻿using Core.CommandProcessorSupport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConferenceApp.UI.Features.Conference
{
    public class AddEditQuery : ICommand
    {
        public Guid Id { get; set; }
    }
}