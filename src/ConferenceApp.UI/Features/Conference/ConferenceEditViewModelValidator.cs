﻿using System;
using System.Web.Mvc;
using FluentValidation;
using NHibernate;

namespace ConferenceApp.UI.Features.Conference
{
    public class ConferenceEditViewModelValidator : AbstractValidator<ConferenceEditViewModel>
    {
        private readonly ISession _conferenceRepository;

        public ConferenceEditViewModelValidator(ISession session)
        {
            _conferenceRepository = session;
            CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleFor(x => x.ConferenceName)
                .NotEmpty()
                .Must(BeAUniqueName)
                .WithMessage("A conference with the same name already exists");

            RuleFor(x => x.HashTag)
                .NotEmpty();

            RuleFor(x => x.StartDate)
                .NotEmpty();

            RuleFor(x => x.EndDate)
                .NotEmpty()
                .GreaterThanOrEqualTo(x => x.StartDate)
                .WithMessage("{PropertyName} must be after or equal to {ComparisonValue}.", x => x.StartDate);

            RuleFor(x => x.Cost)
                .NotEmpty()
                .GreaterThanOrEqualTo(0).WithMessage("{PropertyName} must be at least $0.00");
        }


        public bool BeAUniqueName(ConferenceEditViewModel model, string name)
        {
            //existing model
            if (model.Id != null)
            {
                var conference = _conferenceRepository.Get<Core.Domain.Conference>(model.Id);
                if (conference.Name.ToLower() == model.ConferenceName.ToLower())
                {
                    return true;
                }
            }

            return _conferenceRepository.QueryOver<Core.Domain.Conference>().Where(x => x.Name == model.ConferenceName).RowCount() == 0;
        }
    }
}