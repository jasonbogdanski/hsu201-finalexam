﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using ConferenceApp.UI.Infrastructure.AutoMapper;
using Core.CommandProcessorSupport;
using NHibernate;

namespace ConferenceApp.UI.Features.Conference
{
    using Core.Domain;

    public class AddEditQueryHandler:ICommandHandler<AddEditQuery, ConferenceEditViewModel>
    {
        private readonly ISession _session;

        public AddEditQueryHandler(ISession session)
        {
            _session = session;
        }

        public ConferenceEditViewModel Handle(AddEditQuery viewModel)
        {
            var model = new ConferenceEditViewModel();
            if (viewModel.Id != Guid.Empty)
            {
                model = Mapper.Map<Conference, ConferenceEditViewModel>(_session.Get<Conference>(viewModel.Id));
            }

            return model;
        }
    }
}