﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Core.CommandProcessorSupport;

namespace ConferenceApp.UI.Features.Conference
{
    public class ConferenceIndexViewModel: ICommandResult
    {
        public IList<ConferenceSummaryViewModel> Details { get; set; } 
    }
}