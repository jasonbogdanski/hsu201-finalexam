﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;

namespace ConferenceApp.UI.Features.Conference
{
    public class MappingProfile : Profile
    {
        protected override void Configure()
        {
            base.Configure();

            Mapper.CreateMap<Core.Domain.Conference, ConferenceSummaryViewModel>()
                .ForMember(dest => dest.ConferenceName, opt => opt.MapFrom(src => src.Name));
            Mapper.CreateMap<Core.Domain.Conference, ConferenceEditViewModel>()
                .ForMember(dest => dest.ConferenceName, opt => opt.MapFrom(src => src.Name));
        }
    }
}