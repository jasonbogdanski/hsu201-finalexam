﻿using System.Collections.Generic;
using Core.CommandProcessorSupport;

namespace ConferenceApp.UI.Features.Conference
{
    public class ConferenceBulkEditViewModel : ICommandResult, ICommand
    {
        public IList<ConferenceEditViewModel> Details { get; set; }
    }
}