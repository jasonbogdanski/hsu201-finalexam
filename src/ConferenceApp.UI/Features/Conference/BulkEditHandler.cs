﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Core.CommandProcessorSupport;

namespace ConferenceApp.UI.Features.Conference
{
    public class BulkEditHandler : ICommandHandler<ConferenceBulkEditViewModel, ConferenceBulkEditViewModel>
    {
        private readonly ICommandHandler<ConferenceEditViewModel, ConferenceEditViewModel> _addEditHandler;

        public BulkEditHandler(ICommandHandler<ConferenceEditViewModel, ConferenceEditViewModel> addEditHandler)
        {
            _addEditHandler = addEditHandler;
        }

        public ConferenceBulkEditViewModel Handle(ConferenceBulkEditViewModel viewModel)
        {
            foreach (var editViewModel in viewModel.Details)
            {
                 _addEditHandler.Handle(editViewModel);
            }

            return viewModel;
        }
    }
}