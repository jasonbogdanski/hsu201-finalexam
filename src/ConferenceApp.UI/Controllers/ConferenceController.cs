﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ConferenceApp.UI.Features.Conference;
using Core.CommandProcessorSupport;

namespace ConferenceApp.UI.Controllers
{
    public class ConferenceController : Controller
    {
        private readonly ICommandHandler<IndexQuery, ConferenceIndexViewModel> _indexQueryHandler;
        private readonly ICommandHandler<ConferenceEditViewModel, ConferenceEditViewModel> _addEditHandler;
        private readonly ICommandHandler<AddEditQuery, ConferenceEditViewModel> _addEditQueryHandler;
        private readonly ICommandHandler<BulkEditQuery, ConferenceBulkEditViewModel> _bulkEditQueryHandler;
        private readonly ICommandHandler<ConferenceBulkEditViewModel, ConferenceBulkEditViewModel> _bulkEditHandler;

        public ConferenceController(ICommandHandler<IndexQuery, ConferenceIndexViewModel> indexQueryHandler,
            ICommandHandler<ConferenceEditViewModel, ConferenceEditViewModel> addEditHandler,
            ICommandHandler<AddEditQuery, ConferenceEditViewModel> addEditQueryHandler,
            ICommandHandler<BulkEditQuery, ConferenceBulkEditViewModel> bulkEditQueryHandler,
            ICommandHandler<ConferenceBulkEditViewModel, ConferenceBulkEditViewModel> bulkEditHandler)
        {
            _indexQueryHandler = indexQueryHandler;
            _addEditHandler = addEditHandler;
            _addEditQueryHandler = addEditQueryHandler;
            _bulkEditQueryHandler = bulkEditQueryHandler;
            _bulkEditHandler = bulkEditHandler;
        }

        // GET: Conference
        [HttpGet]
        public ActionResult Index()
        {
            return View(_indexQueryHandler.Handle(new IndexQuery()));
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View(new ConferenceEditViewModel());
        }

        [HttpPost]
        public ActionResult Add(ConferenceEditViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                _addEditHandler.Handle(viewModel);
                return RedirectToAction("Index");
            }

            return View(viewModel);
        }

        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            return View(_addEditQueryHandler.Handle(new AddEditQuery { Id = id }));
        }

        [HttpPost]
        public ActionResult Edit(ConferenceEditViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                _addEditHandler.Handle(viewModel);
                return RedirectToAction("Index");
            }

            return View(viewModel);
        }

        [HttpGet]
        public ActionResult BulkEdit()
        {
            return View(_bulkEditQueryHandler.Handle(new BulkEditQuery()));
        }

        [HttpPost]
        public ActionResult BulkEdit(ConferenceBulkEditViewModel conferenceBulkEditView)
        {
            if (ModelState.IsValid)
            {
                _bulkEditHandler.Handle(conferenceBulkEditView);
                return RedirectToAction("Index");
            }

            return View(conferenceBulkEditView);
        }
    }
}