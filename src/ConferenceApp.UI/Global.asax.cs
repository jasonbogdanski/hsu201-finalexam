﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using ConferenceApp.UI.App_Start;
using ConferenceApp.UI.Infrastructure.AutoMapper;
using ConferenceApp.UI.Infrastructure.FluentValidation;
using FluentValidation;
using FluentValidation.Mvc;
using NHibernate;
using StructureMap;

namespace ConferenceApp.UI
{
    public class MvcApplication : System.Web.HttpApplication
    {

        protected void Application_Start()
        {
            AutoMapperBootStrapper.Initialize();
            DataAnnotationsModelValidatorProvider.AddImplicitRequiredAttributeForValueTypes = false;
            FluentValidationModelValidatorProvider.Configure(provider =>
            {
                provider.ValidatorFactory = StructuremapMvc.Container.GetInstance<FluentValidationFactory>();
            });
            ValidatorOptions.ResourceProviderType = typeof(FluentValidationConfigResource);
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        public class FluentValidationConfigResource
        {
            public static string notempty_error
            {
                get { return "{PropertyName} is Required"; }
            }
        }
    }
}
