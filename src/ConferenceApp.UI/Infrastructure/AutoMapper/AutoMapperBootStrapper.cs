﻿using System;
using System.Linq;
using ConferenceApp.UI.Features.Conference;

namespace ConferenceApp.UI.Infrastructure.AutoMapper
{
    using global::AutoMapper;

    public static class AutoMapperBootStrapper
    {
        private static readonly Lazy<bool> _lazy = new Lazy<bool>(InitializeInternal);

        public static bool Initialize()
        {
            return _lazy.Value;
        }

        private static bool InitializeInternal()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<MappingProfile>();

                foreach (var profileType in typeof(MvcApplication).Assembly.GetTypes()
                    .Where(t => typeof(Profile).IsAssignableFrom(t))
                    .Where(t => t != typeof(MappingProfile))
                    )
                {
                    cfg.AddProfile((Profile)Activator.CreateInstance(profileType));
                }

            });

            return true;
        }
    }
}