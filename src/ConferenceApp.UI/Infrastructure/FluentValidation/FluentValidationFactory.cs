﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConferenceApp.UI.App_Start;
using ConferenceApp.UI.Features.Conference;
using FluentValidation;
using StructureMap;

namespace ConferenceApp.UI.Infrastructure.FluentValidation
{
    public class FluentValidationFactory : ValidatorFactoryBase
    {
        public override IValidator CreateInstance(Type type)
        {
            return StructuremapMvc.Container.TryGetInstance(type) as IValidator;
        }
    }

}