﻿using Core.Infrastructure.Persistence;
using NHibernate;
using NHibernate.Cfg;
using StructureMap.Configuration.DSL;
using StructureMap.Web;

namespace ConferenceApp.UI.DependencyResolution
{
    public class NHibernateRegistry : Registry
    {
        public NHibernateRegistry()
        {
            For<Configuration>().Singleton().Use(c => new ConfigurationFactory().CreateConfiguration());
            For<ISessionFactory>().Singleton().Use(c => c.GetInstance<Configuration>().BuildSessionFactory());
            For<ISession>().HybridHttpOrThreadLocalScoped().Use(c => c.GetInstance<ISessionFactory>().OpenSession());
        }
    }
}