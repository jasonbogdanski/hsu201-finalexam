// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DefaultRegistry.cs" company="Web Advanced">
// Copyright 2012 Web Advanced (www.webadvanced.com)
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using ConferenceApp.UI.Features.Conference;
using ConferenceApp.UI.Infrastructure.FluentValidation;
using Core.Infrastructure.Persistence;
using NHibernate;
using StructureMap.Web;

namespace ConferenceApp.UI.DependencyResolution
{
    using StructureMap.Configuration.DSL;
    using StructureMap.Graph;
    using Core.CommandProcessorSupport;
    using NHibernate.Cfg;

    public class DefaultRegistry : Registry
    {
        #region Constructors and Destructors

        public DefaultRegistry()
        {
            Scan(
                scan =>
                {
                    scan.TheCallingAssembly();
                    scan.LookForRegistries();
                    scan.WithDefaultConventions();    
                    scan.With(new ControllerConvention());
                });

            For<ICommandHandler<ConferenceEditViewModel, ConferenceEditViewModel>>().Use<AddEditHandler>();
            For<ICommandHandler<IndexQuery, ConferenceIndexViewModel>>().Use<IndexQueryHandler>();
            For<ICommandHandler<AddEditQuery, ConferenceEditViewModel>>().Use<AddEditQueryHandler>();
            For<ICommandHandler<BulkEditQuery, ConferenceBulkEditViewModel>>().Use<BulkEditQueryHandler>();
            For<ICommandHandler<ConferenceBulkEditViewModel, ConferenceBulkEditViewModel>>().Use<BulkEditHandler>();
        }

        #endregion
    }
}