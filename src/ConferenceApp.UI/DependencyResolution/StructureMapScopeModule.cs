using System;
using ConferenceApp.UI.App_Start;
using NHibernate;
using System.Web;
using System.Web.Mvc;

namespace ConferenceApp.UI.DependencyResolution {
    using System.Web;

    using ConferenceApp.UI.App_Start;

    using StructureMap.Web.Pipeline;

    public class StructureMapScopeModule : IHttpModule {
        #region Public Methods and Operators

        public void Dispose() {
        }

        public void Init(HttpApplication context) {
            context.BeginRequest += (sender, e) =>
            {
                BeginRequest(sender, e);
                StructuremapMvc.StructureMapDependencyScope.CreateNestedContainer();
            };
            context.EndRequest += (sender, e) => {
                EndRequest(sender, e);
                HttpContextLifecycle.DisposeAndClearAll();
                StructuremapMvc.StructureMapDependencyScope.DisposeNestedContainer();
            };
        }

        void BeginRequest(object sender, EventArgs e)
        {
            FirstRequestInitialisation.Initialize(HttpContext.Current);

            var session = StructuremapMvc.Container.GetInstance<ISession>();
            session.BeginTransaction(System.Data.IsolationLevel.Snapshot);
            HttpContext.Current.Items["NHibernateSession"] = session;
        }

        void EndRequest(object sender, EventArgs e)
        {
            using (var session = (ISession)HttpContext.Current.Items["NHibernateSession"])
            {
                if (HttpContext.Current.Server.GetLastError() != null)
                {
                    if (IsSessionTransactionActive(session))
                        session.Transaction.Rollback();
                    return;
                }

                if (IsSessionTransactionActive(session))
                    session.Transaction.Commit();
            }
        }

        #endregion

        private bool IsSessionTransactionActive(ISession session)
        {
            return session != null && session.Transaction != null && session.Transaction.IsActive;
        }

        static class FirstRequestInitialisation
        {
            private static bool _initialized;

            private static readonly Object Lock = new Object();

            public static void Initialize(HttpContext context)
            {
                if (!_initialized)
                {
                    lock (Lock)
                    {
                        if (_initialized) return;
                        _initialized = true;
                    }
                }
            }
        }
    }
}