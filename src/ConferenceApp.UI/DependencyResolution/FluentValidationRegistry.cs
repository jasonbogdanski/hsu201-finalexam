﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ConferenceApp.UI.Features.Conference;
using StructureMap.Configuration.DSL;
using FluentValidation;

namespace ConferenceApp.UI.DependencyResolution
{
    public class FluentValidationRegistry:Registry
    {
        public FluentValidationRegistry()
        {
            AssemblyScanner.FindValidatorsInAssemblyContaining<ConferenceEditViewModelValidator>()
                 .ForEach(result =>
                 {
                     For(result.InterfaceType)
                        .Use(result.ValidatorType);
                 });
        }
    }
}