﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Domain;
using NHibernate.SqlCommand;

namespace Core.CommandProcessorSupport
{
    public interface ICommandProcessor
    {
        ReturnValue Process<TCommand>(TCommand command) where TCommand : ICommand;
    }

    public interface ICommand
    {

    }

    public interface ICommandResult
    {
        
    }

    public interface ICommandHandler<in TCommand, out TResponse>
        where TCommand : ICommand
        where TResponse : ICommandResult
    {
        TResponse Handle(TCommand viewModel);
    }
}
