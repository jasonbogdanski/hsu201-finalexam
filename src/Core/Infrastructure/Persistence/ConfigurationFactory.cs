﻿using System.Reflection;
using NHibernate.Cfg;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Dialect;
using NHibernate.Driver;
using NHibernate.Mapping.ByCode;
using Environment = System.Environment;

namespace Core.Infrastructure.Persistence
{
    public class ConfigurationFactory
    {
        public Configuration CreateConfiguration()
        {
            var nhConfiguration = ConfigureNHibernate();
            var mappings = GenerateMappings();
            nhConfiguration.AddDeserializedMapping(mappings, "ConferenceApp");
            return nhConfiguration;
        }

        private HbmMapping GenerateMappings()
        {
            var mapper = new DomainMapper();
            mapper.AddMappings(Assembly.GetExecutingAssembly().GetExportedTypes());
            return mapper.CompileMappingForAllExplicitlyAddedEntities();
        }

        private Configuration ConfigureNHibernate()
        {
            var configure = new Configuration();
            configure.SessionFactoryName("ConferenceApp");

            configure.DataBaseIntegration(db =>
            {
                db.Dialect<MsSql2008Dialect>();
                db.Driver<SqlClientDriver>();
                //                db.IsolationLevel = IsolationLevel.Snapshot;
                if (Environment.GetEnvironmentVariable("db_connstring") != null)
                {
                    db.ConnectionString = Environment.GetEnvironmentVariable("db_connstring");
                }
                else
                {
                    db.ConnectionStringName = "AppConnString";
                }
                db.Timeout = 10;
                db.KeywordsAutoImport = Hbm2DDLKeyWords.AutoQuote;
                db.LogFormattedSql = true;
            });

            AdditionalConfiguration(configure);

            return configure;
        }

        protected virtual void AdditionalConfiguration(Configuration configuration)
        {
            // specialized subclasses can do additional configuration here.
        }
    }
}
