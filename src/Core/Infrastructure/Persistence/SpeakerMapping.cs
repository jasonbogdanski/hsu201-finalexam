﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Domain;

namespace Core.Infrastructure.Persistence
{
    public class SpeakerMapping: PersistentObjectMapping<Speaker>
    {
        public SpeakerMapping()
        {
            Property(a => a.FirstName);
            Property(a => a.LastName);
            Property(a => a.Bio);
        }
    }
}
