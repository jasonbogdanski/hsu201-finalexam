﻿using Core.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace Core.Infrastructure.Persistence
{
    public class PersistentObjectMapping<TPersistentObject> : ClassMapping<TPersistentObject>
    where TPersistentObject : PersistentObject
    {
        public PersistentObjectMapping()
        {
            Schema("dbo");
            Id(x => x.Id);
        }
    }
}
