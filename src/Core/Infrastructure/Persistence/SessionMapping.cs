﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Domain;
using NHibernate.Mapping.ByCode;

namespace Core.Infrastructure.Persistence
{
    public class SessionMapping: PersistentObjectMapping<Session>
    {
        public SessionMapping()
        {
            Property(a =>a.Abstract);
            Property(a =>a.Title);
            ManyToOne(a =>a.Speaker, m =>
            {
                m.Cascade(Cascade.Persist);
                m.Lazy(LazyRelation.NoLazy);
            });
        }
    }
}
