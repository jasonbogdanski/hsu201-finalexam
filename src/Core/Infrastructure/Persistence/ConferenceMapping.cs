﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Domain;
using NHibernate.Criterion;

namespace Core.Infrastructure.Persistence
{
    public class ConferenceMapping : PersistentObjectMapping<Conference>
    {
        public ConferenceMapping()
        {
            Property(a => a.Name);
            Property(a => a.StartDate);
            Property(a => a.EndDate);
            Property(a => a.AttendeeCount);
            Property(a => a.SessionCount);
            Property(a => a.Cost);
            Property(a => a.HashTag);
            Set(m => m.Sessions, _ => { }, r => r.OneToMany());
            Set(m => m.Attendees, _ => { }, r => r.OneToMany());
        }
    }
}
