﻿using System;
using System.Linq;
using NHibernate.Mapping.ByCode;

namespace Core.Infrastructure.Persistence
{
    public class DomainMapper : ModelMapper
    {
        public DomainMapper()
        {
            BeforeMapClass += MapperOnBeforeMapClass;
            BeforeMapManyToOne += (inspector, member, customizer) =>
            {
                customizer.Column(member.LocalMember.Name + "Id");
                var containerType = member.GetContainerEntity(inspector);
                customizer.ForeignKey("FK_" + containerType.Name + "_" +
                                      member.LocalMember.GetPropertyOrFieldType().Name + "_" + member.LocalMember.Name);
                customizer.Index("IX_" + containerType.Name + "_" + member.LocalMember.GetPropertyOrFieldType().Name);

            };
            BeforeMapList += (inspector, member, customizer) =>
            {
                customizer.Access(Accessor.Field);
                customizer.Cascade(Cascade.Persist);
                customizer.Key(m =>
                {
                    var containerType = member.GetContainerEntity(inspector);
                    m.ForeignKey("FK_" + member.CollectionElementType().Name + "_" + containerType.Name + "_" +
                                 member.LocalMember.Name);
                    m.Column(cm =>
                    {
                        cm.Name(DetermineKeyColumnName(inspector, member));
                        cm.Index("IX_" + member.CollectionElementType().Name + "_" + containerType.Name);
                    });
                });
            };
            BeforeMapSet += (inspector, member, customizer) =>
            {
                customizer.Access(Accessor.Field);
                customizer.Cascade(Cascade.Persist);
                customizer.Key(m =>
                {
                    var containerType = member.GetContainerEntity(inspector);
                    m.ForeignKey("FK_" + member.CollectionElementType().Name + "_" + containerType.Name + "_" +
                                 member.LocalMember.Name);
                    m.Column(cm =>
                    {
                        cm.Name(DetermineKeyColumnName(inspector, member));
                        cm.Index("IX_" + member.CollectionElementType().Name + "_" + containerType.Name);
                    });
                });
            };
            BeforeMapOneToMany +=
                (inspector, member, customizer) =>
                {
                    customizer.Class(member.LocalMember.GetPropertyOrFieldType().GetGenericArguments().Single());
                };
            BeforeMapManyToMany += (inspector, member, customizer) =>
            {
                var containerType = member.GetContainerEntity(inspector);
                customizer.ForeignKey("FK_" + containerType.Name + "_" + member.CollectionElementType().Name + "_" +
                                      member.LocalMember.Name);
            };

        }

        private static string DetermineKeyColumnName(IModelInspector inspector, PropertyPath member)
        {
            var otherSideProperty = member.OneToManyOtherSideProperty();
            if (inspector.IsOneToMany(member.LocalMember) && otherSideProperty != null)
                return otherSideProperty.Name + "Id";

            return member.Owner().Name + "Id";
        }

        private void MapperOnBeforeMapClass(IModelInspector modelInspector, Type type,
            IClassAttributesMapper classCustomizer)
        {
            classCustomizer.Id(m => m.Generator(Generators.GuidComb));
        }


    }
}
