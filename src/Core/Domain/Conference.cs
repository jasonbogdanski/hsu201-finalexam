﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain
{
    public class Conference : PersistentObject
    {
        private ISet<Session> _sessions = new HashSet<Session>();
        private ISet<Attendee> _attendees = new HashSet<Attendee>();

        public virtual string Name { get; set; }
        public virtual string HashTag { get; set; }
        public virtual DateTime? StartDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
        public virtual decimal Cost { get; set; }
        public virtual int AttendeeCount { get; set; }
        public virtual int SessionCount { get; set; }

        public virtual ISet<Session> Sessions
        {
            get
            {
                return _sessions;
            }
            set { _sessions = value; }
        }

        public virtual ISet<Attendee> Attendees
        {
            get
            {
                return _attendees;
            }
            set { _attendees = value; }
        }

        public virtual void AddSession(Session session)
        {
            _sessions.Add(session);
            ++SessionCount;
        }

        public virtual Attendee Register(string firstName, string lastName)
        {
            var attendee = new Attendee(firstName, lastName);
            _attendees.Add(attendee);
            ++AttendeeCount;
            return attendee;
        }

        public virtual Attendee FindAttendee(Guid attendeeId)
        {
            return Attendees.FirstOrDefault(a => a.Id == attendeeId);
        }

        public virtual void ChangeName(string name)
        {
            if (name == null)
                throw new ArgumentNullException("name");

            if (name == string.Empty)
                throw new ArgumentOutOfRangeException("name", "Must be a non-empty string.");

            Name = name;
        }

        public virtual void ChangeHashTag(string hashTag)
        {
            if (hashTag == null)
                throw new ArgumentNullException("hashTag");

            if (hashTag == string.Empty)
                throw new ArgumentOutOfRangeException("hashTag", "Must be a non-empty string.");

            HashTag = hashTag;
        }

        public virtual void ChangeDates(DateTime startDate, DateTime endDate)
        {
            if (endDate < startDate)
                throw new ArgumentOutOfRangeException("endDate", "Must be on or after startDate");

            StartDate = startDate;
            EndDate = endDate;
        }

        public virtual void ChangeCost(decimal cost)
        {
            Cost = cost;
        }
    }
}

