﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain
{
    public class Session : PersistentObject
    {
        protected Session() { }

        public Session(string title, string @abstract, Speaker speaker)
        {
            Title = title;
            Abstract = @abstract;
            Speaker = speaker;
        }

        public virtual string Title { get; set; }
        public virtual string Abstract { get; set; }
        public virtual Speaker Speaker { get; set; }
    }
}
