﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain
{
    public class ReturnValue
    {
        public ReturnValue()
        {
            Success = true;
        }

        public bool Success { get; set; }

        public Type Type { get; set; }

        public object Value { get; set; }

        public ReturnValue SetValue<T>(T input)
        {
            Type = typeof(T);
            Value = input;
            return this;
        }
    }
}
