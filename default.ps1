$script:project_config = "Release"
$script:datascripts = $FALSE

properties {
	$project_name = "ConferenceApp"
	if(-not $version)
    {
        $version = "0.0.0.1"
    }
	
    $ReleaseDotNumber = if ($env:release_dotnumber) { $env:release_dotnumber } else { "3" }
    $ReleaseSecondDotNumber = if ($env:release_seconddotnumber) { $env:release_seconddotnumber } else { "0" }
	$ReleaseNumber =  if ($env:BUILD_NUMBER) {"$ReleaseDotNumber.$ReleaseSecondDotNumber.$env:BUILD_NUMBER.0"} else {$version}
	
	Write-Host "**********************************************************************"
	Write-Host "Release Number: $ReleaseNumber"
	Write-Host "**********************************************************************"
	
	$OctopusEnvironment =  $env:OCTOPUS_ENVIRONMENT
	$OctopusProjectName = $env:OCTOPUS_PROJECT_NAME

	$base_dir = resolve-path .
	$build_dir = "$base_dir\build"
	$package_dir = "$build_dir\latestVersion"
	$package_file = "$package_dir\" + $project_name + "_Package.zip"
	$source_dir = "$base_dir\src"
	$test_dir = "$build_dir\test"
	$result_dir = "$build_dir\results"

    $db_server = if ($env:db_server) { $env:db_server } else { ".\SqlExpress" }
	
	$test_assembly_patterns_unit = @("*UnitTests.dll")
	$test_assembly_patterns_integration = @("*IntegrationTests*.dll")
    $test_assembly_to_exclude = "IntegrationTests.Common.dll"
	$test_assembly_patterns_full = @("*FullSystemTests.dll")

	$cassini_exe = 'C:\Program Files (x86)\Common Files\microsoft shared\DevServer\11.0\WebDev.WebServer40.EXE'
	$port = 2627

    $conferenceapp_db_name = if ($env:db_name) { $env:db_name } else { "ConferenceApp" }
    $conferenceapp_db_scripts_dir = "$source_dir\DatabaseMigration\Databases\ConferenceApp"

	$conferenceapp_test_db_name = if ($env:db_name) { $env:db_name + "Test" } else { "ConferenceAppTest" }
    $conferenceapp_test_db_scripts_dir = "$source_dir\DatabaseMigration\Databases\ConferenceApp"

    $roundhouse_dir = "$base_dir\tools\roundhouse"
    $roundhouse_output_dir = "$roundhouse_dir\output"
    $roundhouse_exe_path = "$roundhouse_dir\rh.exe"
    $roundhouse_local_backup_folder = "$base_dir\database_backups"
	
	<# Added by Eduardo #>
 
	$octopus_API_URL = if ($env:octopus_API_URL) { $env:octopus_API_URL } else {  "http://deploy.conferenceapp.org:81/api" }
	$octopus_API_key = if ($env:octopus_API_key) { $env:octopus_API_key } else { "ASDFASDFADSFADFSADFSADFS" } 
	$octopus_nuget_repo = "C:\Nugets"
	
    $all_database_info = @{
        "$conferenceapp_db_name"="$conferenceapp_db_scripts_dir"
		"$conferenceapp_test_db_name"="$conferenceapp_test_db_scripts_dir"
    }
	
}

#These are aliases for other build tasks. They typically are named after the camelcase letters (rad = Rebuild All Databases)
#aliases should be all lowercase, conventionally
#please list all aliases in the help task
task default -depends InitialPrivateBuild
task dev -depends EnableDataScripts, DeveloperBuild
task ci -depends IntegrationBuild
task uad -depends UpdateAllDatabases
task rad -depends EnableDataScripts, RebuildAllDatabases
task tq -depends RunIntegrationTestsQuickly
task tt -depends RunIntegrationTestsThoroughly
task unit -depends RunAllUnitTests
task quick -depends QuickRebuild
#task ld -depends LoadSampleData
#task rld -depends ReLoadSampleData
#task compdb -depends RebuildAllComparisonDatabases

task help {
	Write-Help-Header
	Write-Help-Section-Header "Comprehensive Building"
	Write-Help-For-Alias "(default)" "Intended for first build or when you want a fresh, clean local copy"
	Write-Help-For-Alias "dev" "Optimized for local dev; Most noteably UPDATES databases instead of REBUILDING"
	Write-Help-For-Alias "ci" "Continuous Integration build (long and thorough) with packaging"
	Write-Help-For-Alias "quick" "Compile and update the database, but skip tests"
	Write-Help-Section-Header "Database Maintence"
	Write-Help-For-Alias "uad" "Update All the Databases to the latest version (all db used for the app, that is)"
	Write-Help-For-Alias "rad" "Rebuild All the Databases to the latest version from scratch (useful while working on the schema)"
	#Write-Help-For-Alias "compdb" "Rebuild all the comparison databases from scratch (to use for schema comparison)"
	Write-Help-Section-Header "Running Tests"
	Write-Help-For-Alias "unit" "All unit tests"
	Write-Help-For-Alias "tq" "Unit and Test Integration Quickly, aka, UPDATE databases before testing"
	Write-Help-For-Alias "tt" "Unit and Test Integration Thoroughly, aka, REBUILD databases before testing (useful while working on the schema)"
	#Write-Help-Section-Header "Sample Data Management"
	#Write-Help-For-Alias "ld" "Load Developer Data"
	#Write-Help-For-Alias "rld" "Load Developer Data, after full rebuilding all the databases, aka, 'clean' reload"
	Write-Help-Footer
	exit 0
}

#These are the actual build tasks. They should be Pascal case by convention
task InitialPrivateBuild -depends StopCassini, Clean, <#, ReLoadSampleData#> Compile, RebuildAllDatabases, RunAllUnitTests, RunIntegrationTestsThoroughly , WarnSlowBuild

task DeveloperBuild -depends StopCassini, SetDebugBuild, Clean, Compile, UpdateAllDatabases, RunAllUnitTests, RunIntegrationTestsQuickly

task IntegrationBuild -depends StopCassini, SetReleaseBuild, CommonAssemblyInfo, Clean, Compile, RebuildAllDatabases, RunAllUnitTests, RunIntegrationTestsThoroughly #, GenerateNugetPackage, CreateOctopusRelease

task ReleaseBuild -depends StopCassini, SetReleaseBuild, CommonAssemblyInfo, Clean, Compile, Package

task QuickRebuild -depends StopCassini, SetDebugBuild, Clean, Compile, UpdateAllDatabases

task SetDebugBuild {
    $script:project_config = "Debug"
}

task SetReleaseBuild {
    $script:project_config = "Release"
}

task EnableDataScripts {
	$script:datascripts = $TRUE
}

task ConfigureLocalIIS {
    exec { & C:\Windows\System32\inetsrv\appcmd.exe "unlock" "config" "-section:system.webServer/security/authentication/windowsAuthentication" }
    exec { & C:\Windows\System32\inetsrv\appcmd.exe "unlock" "config" "-section:system.webServer/security/authorization" }
    exec { & "c:\Program Files (x86)\IIS Express\appcmd" "unlock" "config" "-section:system.webServer/security/authentication/windowsAuthentication" }
    exec { & "c:\Program Files (x86)\IIS Express\appcmd" "unlock" "config" "-section:system.webServer/security/authorization" }
}

task RebuildAllDatabases {
    $all_database_info.GetEnumerator() | %{ 
		Write-Host $_.Key
		deploy-database "Rebuild" $db_server $_.Key $_.Value
	}
}

task UpdateAllDatabases {
    $all_database_info.GetEnumerator() | %{ deploy-database "Update" $db_server $_.Key $_.Value}
}

task RebuildAllComparisonDatabases {
    $all_database_info.GetEnumerator() | %{ deploy-database "Rebuild" $db_server ($_.Key + "_Comp") $_.Value}
}

task ConfigureIis {
    Import-Module WebAdministration
    New-Item 'IIS:\Sites\Default Web Site\ConferenceApp' -physicalPath "$source_dir\Conference.UI" -type Application -force
    exit 0
}

task Casper {
    $phantomjs_path = join-path $build_dir "..\tools\phantomjs" -resolve
    $Env:Path += ";$phantomjs_path"

    $test_path = join-path $build_dir "..\src\IntegrationTests\UI\" -resolve

    exec { .\tools\casperjs\batchbin\casperjs.bat test $test_path }
}

task GruntCi {
    #$nodejs_path = join-path $build_dir "..\tools\nodejs" -resolve
    #$Env:Path += ";$nodejs_path"
    exec { .\grunt.cmd ci --no-color }
}

task GruntDev {
    #$nodejs_path = join-path $build_dir "..\tools\nodejs" -resolve
    #$Env:Path += ";$nodejs_path"
    exec { .\grunt.cmd dev -- no-color }
}

#task LoadSampleData -depends Compile, UpdateAllDatabases {
#	exec { & $source_dir\DataLoader\bin\$project_config\DataLoader.exe }
#}

#task ReLoadSampleData -depends Compile, RebuildAllDatabases {
#	exec { & $source_dir\DataLoader\bin\$project_config\DataLoader.exe }
#}

task CommonAssemblyInfo {
    create-commonAssemblyInfo "$ReleaseNumber" $project_name "$source_dir\SharedAssemblyInfo.cs"
}

task CopyAssembliesForTest -Depends Compile {
    copy_all_assemblies_for_test $test_dir
}

task RunIntegrationTestsThoroughly -Depends CopyAssembliesForTest, RebuildAllDatabases {
    $test_assembly_patterns_integration | %{ run_fixie_tests $_ }
}

task RunIntegrationTestsQuickly -Depends CopyAssembliesForTest, UpdateAllDatabases {
    $test_assembly_patterns_integration | %{ run_fixie_tests $_ }
}

task RunAllUnitTests -Depends CopyAssembliesForTest {
    $test_assembly_patterns_unit | %{ run_fixie_tests $_ }
}

task Compile -depends Clean, CommonAssemblyInfo { 
    exec { & $source_dir\.nuget\nuget.exe restore $source_dir\$project_name.sln }
    exec { msbuild.exe /t:build /v:q /p:Configuration=$project_config /p:Platform="Any CPU" /nologo $source_dir\$project_name.sln }
}

task Clean {
    delete_file $package_file
    delete_directory $build_dir
    create_directory $test_dir 
    create_directory $result_dir
	
    exec { msbuild /t:clean /v:q /p:Configuration=$project_config /p:Platform="Any CPU" $source_dir\$project_name.sln }
}

task Package -depends SetReleaseBuild {
    delete_directory $package_dir
    
    #databases
    #copy_files "$source_dir\database" "$package_dir\database"
    
    #websites
    #copy_website_files "$web_dir" "$package_dir\web\TechShare.prosecutor" @("Logs\log4net.log")
	#	delete_directory "$package_dir\web\TechShare.prosecutor\Logs"
	#	delete_directory "$package_dir\web\TechShare.prosecutor\Files"

    #tools
    #copy_files "$base_dir\tools\tarantino" "$package_dir\tools\tarantino"
	
    #pstrami deployment
    #copy_files "$base_dir\deployment" "$package_dir"

    zip_directory $package_dir $package_file
}

task UserInterfaceTesting -depends StopCassini, Clean, CommonAssemblyInfo, RebuildAllDatabases, Compile {
    start-cassini
    Start-Sleep -Seconds 5 # give time for the system to start
    $test_assembly_patterns_full | %{ run_tests $_ }
    Start-Sleep -Seconds 3
    stop-cassini
}

task StopCassini {
    stop-cassini
}

task GenerateNugetPackage{
    exec { msbuild.exe $source_dir\$project_name.sln /t:build /p:RunOctoPack=true /v:q /p:Configuration=$project_config /p:Platform="Any CPU" /nologo /p:OctoPackPackageVersion=$ReleaseNumber /p:OctoPackPublishPackageToFileShare=$octopus_nuget_repo }  
}


task CreateOctopusRelease {
    $strPath = "$source_dir\ConferenceApp.UI\bin\ConferenceApp.UI.dll"
    exec { octo.exe create-release --project=$OctopusProjectName --server=$octopus_API_URL --apiKey=$octopus_API_key --packageversion=$ReleaseNumber --version=$ReleaseNumber --deployto=$OctopusEnvironment --force=true --waitfordeployment}
}

	
task WarnSlowBuild {
	Write-Host ""
	Write-Host "Warning: " -foregroundcolor Yellow -nonewline;
	Write-Host "The default build you just ran is primarily intended for initial "
	Write-Host "environment setup. While developing you most likely want the quicker dev"
	Write-Host "build task. For a full list of common build tasks, run: "
	Write-Host " > build.bat help"
}

# -------------------------------------------------------------------------------------------------------------
# generalized functions added by Headspring for Help Section
# --------------------------------------------------------------------------------------------------------------

function Write-Help-Header($description) {
	Write-Host ""
	Write-Host "********************************" -foregroundcolor DarkGreen -nonewline;
	Write-Host " HELP " -foregroundcolor Green  -nonewline; 
	Write-Host "********************************"  -foregroundcolor DarkGreen
	Write-Host ""
	Write-Host "This build script has the following common build " -nonewline;
	Write-Host "task " -foregroundcolor Green -nonewline;
	Write-Host "aliases set up:"
}

function Write-Help-Footer($description) {
	Write-Host ""
	Write-Host " For a complete list of build tasks, view default.ps1."
	Write-Host ""
	Write-Host "**********************************************************************" -foregroundcolor DarkGreen
}

function Write-Help-Section-Header($description) {
	Write-Host ""
	Write-Host " $description" -foregroundcolor DarkGreen
}

function Write-Help-For-Alias($alias,$description) {
	Write-Host "  > " -nonewline;
	Write-Host "$alias" -foregroundcolor Green -nonewline; 
	Write-Host " = " -nonewline; 
	Write-Host "$description"
}

# -------------------------------------------------------------------------------------------------------------
# generalized functions 
# --------------------------------------------------------------------------------------------------------------
function deploy-database($action,$server,$db_name,$scripts_dir,$env) {
    $roundhouse_version_file = "$source_dir\ConferenceApp.UI\bin\ConferenceApp.UI.dll"

    if (!$env) {
        $env = "LOCAL"
        Write-Host "RoundhousE environment variable is not specified... defaulting to 'LOCAL'"
    } else {
        Write-Host "Executing RoundhousE for environment:" $env
    }

	if ($action -eq "Update"){
		exec { &$roundhouse_exe_path -s $server -d "$db_name"  --commandtimeout=300 -f $scripts_dir --env $env --silent -o $roundhouse_output_dir --transaction }
	}
	if ($action -eq "Rebuild"){
		exec { &$roundhouse_exe_path -s $server -d "$db_name" --commandtimeout=300 --env $env --silent -drop -o $roundhouse_output_dir }
		exec { &$roundhouse_exe_path -s $server -d "$db_name" --commandtimeout=300 -f $scripts_dir -env $env -vf $roundhouse_version_file --silent --simple -o $roundhouse_output_dir }
	}

}
function start-cassini {
    &$cassini_exe "/port:$port" "/path:$web_dir"
}

function stop-cassini {
    Get-Process | ?{ $_.Name -eq $cassini_process_name } | %{ Stop-Process -Id $_.Id }
}

function run_tests([string]$pattern) {
    
    $items = Get-ChildItem -Path $test_dir $pattern
    $items | %{ run_xunit $_.Name }
}

function run_fixie_tests([string]$pattern) {
    
    $items = Get-ChildItem -Path $test_dir $pattern | Where-Object {$_.Name -NotMatch $test_assembly_to_exclude}
    $items | %{ run_fixie $_.Name }
}

function global:zip_directory($directory,$file) {
    write-host "Zipping folder: " $test_assembly
    delete_file $file
    cd $directory
    & "$base_dir\tools\7zip\7za.exe" a -mx=9 -r $file
    cd $base_dir
}

function global:delete_file($file) {
    if($file) { remove-item $file -force -ErrorAction SilentlyContinue | out-null } 
}

function global:delete_directory($directory_name) {
  rd $directory_name -recurse -force  -ErrorAction SilentlyContinue | out-null
}

function global:create_directory($directory_name) {
  mkdir $directory_name  -ErrorAction SilentlyContinue  | out-null
}

function global:run_xunit ($test_assembly) {
	$assembly_to_test = $test_dir + "\" + $test_assembly
	$results_output = $result_dir + "\" + $test_assembly + ".xml"
    write-host "Running XUnit Tests in: " $test_assembly
    exec { & tools\xunit\xunit.console.clr4.exe $assembly_to_test /silent /nunit $results_output }
}

function global:run_fixie ($test_assembly) {
	$assembly_to_test = $test_dir + "\" + $test_assembly
	$results_output = $result_dir + "\" + $test_assembly + ".xml"
    write-host "Running Fixie Tests in: " $test_assembly
    exec { & tools\fixie\Fixie.Console.exe $assembly_to_test --fixie:NUnitXml $results_output }
}

function global:Copy_and_flatten ($source,$include,$dest) {
	Get-ChildItem $source -include $include -r | cp -dest $dest
}

function global:copy_all_assemblies_for_test($destination){
	$bin_dir_match_pattern = "$source_dir\**\bin\$project_config"
	create_directory $destination
	Copy_and_flatten $bin_dir_match_pattern @("*.exe","*.dll","*.config","*.pdb","*.sql","*.xlsx","*.csv") $destination

	$dictionaryDestination = "$destination\Dictionary"
	create_directory $dictionaryDestination
	Copy_and_flatten $bin_dir_match_pattern @("*.aff", "*.dic") $dictionaryDestination
}

function global:copy_website_files($source,$destination){
    $exclude = @('*.user','*.dtd','*.tt','*.cs','*.csproj') 
    copy_files $source $destination $exclude
	delete_directory "$destination\obj"
}

function global:copy_files($source,$destination,$exclude=@()){    
    create_directory $destination
    Get-ChildItem $source -Recurse -Exclude $exclude | Copy-Item -Destination {Join-Path $destination $_.FullName.Substring($source.length)} 
}

function global:Convert-WithXslt($originalXmlFilePath, $xslFilePath, $outputFilePath) {
   ## Simplistic error handling
   $xslFilePath = resolve-path $xslFilePath
   if( -not (test-path $xslFilePath) ) { throw "Can't find the XSL file" } 
   $originalXmlFilePath = resolve-path $originalXmlFilePath
   if( -not (test-path $originalXmlFilePath) ) { throw "Can't find the XML file" } 
   #$outputFilePath = resolve-path $outputFilePath -ErrorAction SilentlyContinue 
   if( -not (test-path (split-path $originalXmlFilePath)) ) { throw "Can't find the output folder" } 

   ## Get an XSL Transform object (try for the new .Net 3.5 version first)
   $EAP = $ErrorActionPreference
   $ErrorActionPreference = "SilentlyContinue"
   $script:xslt = new-object system.xml.xsl.xslcompiledtransform
   trap [System.Management.Automation.PSArgumentException] 
   {  # no 3.5, use the slower 2.0 one
      $ErrorActionPreference = $EAP
      $script:xslt = new-object system.xml.xsl.xsltransform
   }
   $ErrorActionPreference = $EAP
   
   ## load xslt file
   $xslt.load( $xslFilePath )
     
   ## transform 
   $xslt.Transform( $originalXmlFilePath, $outputFilePath )
}

function global:create-commonAssemblyInfo($version,$applicationName,$filename) {
"using System.Reflection;

// Version information for an assembly consists of the following four values:
//
//      Year                    (Expressed as YYYY)
//      Major Release           (i.e. New Project / Namespace added to Solution or New File / Class added to Project)
//      Minor Release           (i.e. Fixes or Feature changes)
//      Build Date & Revsion    (Expressed as MMDD)
//
[assembly: AssemblyCompany(""Conference for Urban Counties"")]
[assembly: AssemblyCopyright(""Copyright  TechShare 2013"")]
[assembly: AssemblyTrademark("""")]
[assembly: AssemblyCulture("""")]
[assembly: AssemblyVersion(""$version"")]
[assembly: AssemblyFileVersion(""$version"")]" | out-file $filename -encoding "utf8"
}


function script:poke-xml($filePath, $xpath, $value, $namespaces = @{}) {
    [xml] $fileXml = Get-Content $filePath
    
    if($namespaces -ne $null -and $namespaces.Count -gt 0) {
        $ns = New-Object Xml.XmlNamespaceManager $fileXml.NameTable
        $namespaces.GetEnumerator() | %{ $ns.AddNamespace($_.Key,$_.Value) }
        $node = $fileXml.SelectSingleNode($xpath,$ns)
    } else {
        $node = $fileXml.SelectSingleNode($xpath)
    }
    
    Assert ($node -ne $null) "could not find node @ $xpath"
        
    if($node.NodeType -eq "Element") {
        $node.InnerText = $value
    } else {
        $node.Value = $value
    }

    $fileXml.Save($filePath) 
}

function usingx {
    param (
        $inputObject = $(throw "The parameter -inputObject is required."),
        [ScriptBlock] $scriptBlock
    )

    if ($inputObject -is [string]) {
        if (Test-Path $inputObject) {
            [void][system.reflection.assembly]::LoadFrom($inputObject)
        } elseif($null -ne (
              new-object System.Reflection.AssemblyName($inputObject)
              ).GetPublicKeyToken()) {
            [void][system.reflection.assembly]::Load($inputObject)
        } else {
            [void][system.reflection.assembly]::LoadWithPartialName($inputObject)
        }
    } elseif ($inputObject -is [System.IDisposable] -and $scriptBlock -ne $null) {
        Try {
            &$scriptBlock
        } Finally {
            if ($inputObject -ne $null) {
                $inputObject.Dispose()
            }
            Get-Variable -scope script |
                Where-Object {
                    [object]::ReferenceEquals($_.Value.PSBase, $inputObject.PSBase)
                } |
                Foreach-Object {
                    Remove-Variable $_.Name -scope script
                }
        }
    } else {
        $inputObject
    }
} 
